Source: mutt-wizard
Section: mail
Priority: optional
Maintainer: Braulio Henrique Marques Souto <braulio@disroot.org>
Uploaders: Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.2
Homepage: https://github.com/LukeSmithxyz/mutt-wizard
Vcs-Browser: https://salsa.debian.org/debian/mutt-wizard
Vcs-Git: https://salsa.debian.org/debian/mutt-wizard.git

Package: mutt-wizard
Architecture: all
Depends: curl, isync, msmtp, neomutt, pass, procps, xdg-utils, ${misc:Depends}
Recommends: abook, cron, lynx, notmuch, urlview
Suggests: links2, mpop, mpv, w3m, zathura
Description: configuration tool from command line to neomutt
 mutt-wizard is a tool that automatically sets up a neomutt-based minimal
 email system.
 .
 With mutt-wizard you can automatically configure which IMAP and SMTP servers
 you want to use, and also create ready-to-use configuration files (dotfiles)
 for neomutt, isync and msmtp. It can also be used with the POP protocol via
 mpop by those who prefer it.
 .
 Can also handle multiple e-mail accounts and can easily switch between them.
 Sets up sensible defaults, including vim bindings and simple color scheming
 to maximize the usability of neomutt.
 .
 mutt-wizard also provides a mailsync script that can be scheduled to run as
 often as you like, that downloads/syncs mail and notifies you when a new mail
 arrives. Changes standard hotkeys to be more productive and intuitive, and a
 more attractive appearance for neomutt.
